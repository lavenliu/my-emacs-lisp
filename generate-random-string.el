(defun my-insert-random-string (NUM)
  "Insert a random alphanumerics string of length 5.
The possible chars are: 2 to 9, upcase or lowercase English alphabet but no a e i o u, no L l and no 0 1.
Call `universal-argument' before for different count."
  (interactive "P")
  (let* ((xcharset "ABCDFGHJKMNPQRSTVWXYZabcdfghjkmnpqrstvwxyz23456789!@#$%^&*()_")
         (xbaseCount (length xcharset)))
    (dotimes (_ (if (numberp NUM) (abs NUM) 8))
      (insert (elt xcharset (random xbaseCount))))))
