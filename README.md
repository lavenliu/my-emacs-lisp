
# Emacs Lisp

自己常用的 Emacs Lisp 片段。

## Where is Init File

By default, there is no init file. You have to create the file yourself, and add emacs lisp code to it.

Create a file with this content:

```lisp
(set-background-color "ivory")
Save it at ~/.emacs.d/init.el
```

On linux and Mac, by default, emacs look for init file at the following filepath, in order:

- ~/.emacs
- ~/.emacs.el
- ~/.emacs.d/init.el (this is modern, preferred, since ~2010)
- ~/.config/emacs/init.el (linux convention `XDG_CONFIG_HOME` environment variable, supported since Emacs 27 (Released 2020-08).)
